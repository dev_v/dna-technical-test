## Detección de mutaciones genéticas

#### Este proyecto forma parte del desarrollo de una prueba técnica desarrollado por Julio César Cervantes Martínez

## Requerimientos

+ Node.js
+ MongoDB 4.0 o superior

+ ## Configuración
    - El proyecto originalmente está configurado para conectarse al servidor de mongo con los valores por defecto (en la base de datos con nombre mutations) y ejecutar el API en el puerto 3000, estas conexiones pueden editarse en archivo ***.env*** del directorio raíz

# Instalación

Para instalar el proyecto localmente sólo es necesario ejecutar

> npm install

Si los requerimientos han sido cumplidos bastará ejecutar

> npm start

# Pruebas

Se agregó el comando *test* para ejecutar pruebas con los frameworks de testing **mocha** y **supertest**, el mismo comando también ejecuta la herramienta de 
Istanbul Code Coverage **nyc**, para obtener un reporte de cobertura alcanzada por las pruebas.

> npm test

# Integración en la nube
## Host
El host de la aplicación elegido ha sido Heroku, se ha usado el archivo de configuración .gitlab-ci.yml para su Integración continua

+ Enlace del host: https://dna-technical-test.herokuapp.com/
+ Metodo POST mutation: https://dna-technical-test.herokuapp.com/mutation
    + contentType: "application/json"
    + body (ejemplo):
    ```json
    { 
        "dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"] 
    }
    ```
+ Metodo GET stats: https://dna-technical-test.herokuapp.com/stats

## Base de Datos en la nube
Se ha elegido MongoDB Atlas para habilitar un cluster y conectar la aplicación a este servicio, el URI del mismo se agrega cómo una variable de entorno 

