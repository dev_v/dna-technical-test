require('dotenv').config()

const express = require('express'), app = express(), port = process.env.PORT || 3000
const mongoose = require('mongoose');
const helmet = require("helmet");
const ExpressBrute = require('express-brute');
const badRequestMiddleware = require('./middlewares/badRequestMiddleware');

const store = new ExpressBrute.MemoryStore();
const bruteforce = new ExpressBrute(store,{freeRetries:20,minWait:1000}); // Example for protect brute force


mongoose.Promise = global.Promise;
const urlDB = process.env.MONGODB_URI || 'mongodb://localhost:27017/mutations';

mongoose.connect(urlDB, {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true
})
.then(mongoose => console.log('Connecting DB on',urlDB))
.catch(err => console.log(err));


app.use(express.static('public'))
app.use(express.json())
app.use(badRequestMiddleware.handleBadRequest)
app.use(express.urlencoded({extended: true}));
app.use(bruteforce.prevent); //Middleware used globally
app.use(require('./routes'))

app.use(helmet());
/*
...is equivalent to this:
app.use(helmet.contentSecurityPolicy()); //prevent cross-site scripting attacks and other cross-site injections.
app.use(helmet.dnsPrefetchControl()); // attempt to resolve domain names before a user tries to follow a link.
app.use(helmet.expectCt()); //Certificate Transparency
app.use(helmet.frameguard()); //Prevent clickjacking attacks
app.use(helmet.hidePoweredBy()); //Removes the X-Powered-By header, which is set by default on Express. Is mostly removed to save bandwidth.
app.use(helmet.hsts()); //  sets the Strict-Transport-Security header which tells browsers to prefer HTTPS over insecure HTTP
app.use(helmet.ieNoOpen()); //sets the X-Download-Options header, which is specific to Internet Explorer 8. It forces potentially-unsafe downloads to be saved, mitigating execution of HTML in your site's context.
app.use(helmet.noSniff()); // sets the X-Content-Type-Options header to nosniff. This mitigates MIME type sniffing which can cause security vulnerabilities.
app.use(helmet.permittedCrossDomainPolicies()); // sets the X-Permitted-Cross-Domain-Policies header, which tells some clients your domain's policy for loading cross-domain content
app.use(helmet.referrerPolicy()); //  controls what information is set in the Referer header [Referrer-Policy]
app.use(helmet.xssFilter()); // cross-site scripting filter
*/

app.listen(port, () => console.log(`Listening on port ${port}`))

module.exports = app; // For test...