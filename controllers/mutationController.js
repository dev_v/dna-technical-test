const {hasMutation} = require("../helpers/mutations");
const models = require('../models');
module.exports = {
    /**
     * Find dna sequence in database or check if dna has mutation and save result
     */
    mutation: async (req,res,next)=>{
        const dnaString = req.body["dna"].join("");
        
        const exists = await models.DNA.findOne({
            _id: dnaString
        });

        if(exists){
            exists["isMutation"]? res.sendStatus(200):res.sendStatus(403);
        }else{
            if(hasMutation(req.body["dna"])){
                newDNA = await models.DNA.create({_id:dnaString,isMutation:true})
                res.sendStatus(200);
            }else{
                newDNA = await models.DNA.create({_id:dnaString,isMutation:false})
                res.sendStatus(403);
            }  
        }

    },
    /**
     * removes dna sequence saved 
     * this requires req.params.dnaString as a string (["AATTGG","AATTGG"] dna array must first be converted to string, e.g "AATTGGAATTGG" )
     */
    delete: async (req,res,next)=>{
        let deleted = await models.DNA.deleteOne({_id:req.params.dnaString})
        if(deleted){
            res.sendStatus(200);
        }else{
            res.sendStatus(500);
        }
    }
}