const {hasMutation} = require("../helpers/mutations");
const models = require('../models');
module.exports = {
    /**
     * Get JSON mutation stats
     */
    stats: async(req,res,next)=>{
        const count_mutations = await models.DNA.find({ isMutation: true }).countDocuments();
        const count_no_mutation = await models.DNA.find({ isMutation: false }).countDocuments();
        const ratio = count_no_mutation == 0?"no ratio":count_mutations/count_no_mutation;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({
            count_mutations,
            count_no_mutation,
            ratio
        }));
    }
}