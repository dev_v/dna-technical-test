const { Trie, TrieNode } = require("./trie");

/**
 * 
 * @param {Array.<string>} dna - String Array
 * @param {Array.<string>} words - words to find
 * @returns {boolean} Solves if words appear on String Array (like alphabet soup)
 */
function findWords(dna,words){
    let trie = new Trie();
    for(let i=0;i<words.length;i++){
        trie.insert(words[i],i)
    }
    dna = dna.map(el=>el.split``);
    for(let i = 0;i<dna.length;i++){
        for(let j =0;j<dna[i].length;j++){
            if(findWordsHelperV(dna,i,j,trie.root,words)){
                return true;
            }
            if(findWordsHelperH(dna,i,j,trie.root,words)){
                return true;
            }
            if(findWordsHelperD1(dna,i,j,trie.root,words)){
                return true;
            }
            if(findWordsHelperD2(dna,i,j,trie.root,words)){
                return true;
            }
        }
    }
    return false;
}

/**
 * 
 * @param {Array.<string>} dna - dna string array 
 * @param {number} i index row
 * @param {number} j index column
 * @param {TrieNode} root evaluation node
 * @param {Array.<string>} words - muttation patterns
 * @returns {Boolean} Solves if the word appears Vertically (uptodown)
 */
function findWordsHelperV(dna,i,j,root,words){
    if(!Object.keys(root.neighbours).includes(dna[i][j])) return;
    if(root.neighbours[dna[i][j]].isEnd){
        return true
    }// Word founded
    // If current ch is included as neighbour...
    if(i+1 < dna.length){ // Find vertically
        return findWordsHelperV(dna,i+1,j,root.neighbours[dna[i][j]],words)
    }
}

/**
 * 
 * @param {Array.<string>} dna - dna string array 
 * @param {number} i index row
 * @param {number} j index column
 * @param {TrieNode} root evaluation node
 * @param {Array.<string>} words - muttation patterns
 * @returns {Boolean} Solves if the word appears Horizontally (lefttoright)
 */
function findWordsHelperH(dna,i,j,root,words){
    if(!Object.keys(root.neighbours).includes(dna[i][j])) return;
    if(root.neighbours[dna[i][j]].isEnd){
        return true
    }// Word founded
    // If current ch is included as neighbour...
    if(j+1 < dna[0].length){ // Find vertically
        return findWordsHelperH(dna,i,j+1,root.neighbours[dna[i][j]],words)
    }
}

/**
 * 
 * @param {Array.<string>} dna - dna string array 
 * @param {number} i index row
 * @param {number} j index column
 * @param {TrieNode} root evaluation node
 * @param {Array.<string>} words - muttation patterns
 * @returns {Boolean} Solves if the word appears Diagonally (topLeftToBottomRight)
 */
function findWordsHelperD1(dna,i,j,root,words){
    if(!Object.keys(root.neighbours).includes(dna[i][j])) return;
    if(root.neighbours[dna[i][j]].isEnd){
        return true
    }// Word founded
    // If current ch is included as neighbour...
    if(j+1 < dna[0].length && i+1 < dna.length){ // Find vertically
        return findWordsHelperD1(dna,i+1,j+1,root.neighbours[dna[i][j]],words)
    }
}
/**
 * 
 * @param {Array.<string>} dna - dna string array 
 * @param {number} i index row
 * @param {number} j index column
 * @param {TrieNode} root evaluation node
 * @param {Array.<string>} words - muttation patterns
 * @returns {Boolean} Solves if the word appears Diagonally (topRightToBottomLeft)
 */
function findWordsHelperD2(dna,i,j,root,words){
    if(!Object.keys(root.neighbours).includes(dna[i][j])) return;
    if(root.neighbours[dna[i][j]].isEnd){
        return true
    }// Word founded
    // If current ch is included as neighbour...
    if(j-1 >= 0 && i+1 < dna.length){ // Find vertically
        return findWordsHelperD2(dna,i+1,j-1,root.neighbours[dna[i][j]],words)
    }
}

/**
 * 
 * @param {Array.<string>} dna - String Array
 * @returns {boolean} Solves if dna has mutations
 */
function hasMutation(dna){
    let mutationPatterns = ["AAAA","TTTT","CCCC","GGGG"];
    return findWords(dna,mutationPatterns);
}

module.exports = {hasMutation};