class TrieNode{
    /**
     * Create a new Trie node.
     * @param {number} character - value for this node.
     */
    constructor(character){
        this.value = character;
        this.isEnd = false;
        this.neighbours = {};
    }
}

/** Class representing a Trie (prefix tree or digital tree). */
class Trie{
    /**
     * Create a new Trie.
     */
    constructor(){
        this.root = new TrieNode();
    }

    /**
     * Insert a new Node in Trie
     * @param {string} word - new word to add
     * @param {number} index - Index position
     */
    insert(word,index){
        let start = this.root;
        [...word].forEach(ch=>{
            if(!Object.keys(start.neighbours).includes(ch)){
                start.neighbours[ch] = new TrieNode(ch);
            }
            start = start.neighbours[ch];
        })
        start.isEnd = true;
        start.index = index;
    }
}

module.exports = {
    Trie, TrieNode
}