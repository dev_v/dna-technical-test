/**
 * 
 * @param {string} allchars - string to test
 * @param {RegExp} regex  - RegExp instantiation
 * @returns 
 */
function checkStringStructure(allchars,regex){
    return allchars.length > 0 && regex.test(allchars);
}

module.exports = {checkStringStructure};