const {checkStringStructure} = require("../helpers/utils");

module.exports = {
    /**
     * Check dna structure middleware.
     * Ensures that `req.body['dna']` contains only TCGA chars
     */
    checkDNA: async(req, res, next) => {
        if(req.body.hasOwnProperty("dna") && checkStringStructure(req.body["dna"].join(""),new RegExp(/^[\T\G\C\A]*$/gm))){
            next();
        }else{
            return res.status(400).send({
                message: 'La estructura DNA es inválida'
            });
        }
    }
};