const mongoose = require('mongoose');
const { Schema } = mongoose;

const dnaSchema = new Schema({
    _id: {type:String},
    isMutation: {
        type: Boolean
    }
});

dnaSchema.virtual("dnaString").get(function(){
    return this._id;
});

const DNA = mongoose.model('dna', dnaSchema);
module.exports = DNA;