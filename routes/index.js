const express = require('express'), router = express.Router()

router.use('/mutation', require('./mutation'))
router.use('/stats', require('./stats'))

router.get('/', (req, res) => {
  res.sendStatus(501)
})

module.exports = router