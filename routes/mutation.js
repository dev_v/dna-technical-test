const express = require('express'), router = express.Router(), {hasMutation} = require("../helpers/mutations")
const concat = require('concat-stream');
const mutationController = require('../controllers/mutationController');
const mutationMiddleware = require('../middlewares/mutationMiddleware');

router.post('/',mutationMiddleware.checkDNA, mutationController.mutation);
router.delete('/:dnaString', mutationController.delete);

module.exports = router