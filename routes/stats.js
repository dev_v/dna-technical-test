const express = require('express'), router = express.Router()
const statsController = require('../controllers/statsController');

router.get('/', statsController.stats);
module.exports = router