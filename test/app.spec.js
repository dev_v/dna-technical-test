const supertest = require("supertest");
const assert = require('assert');
const app = require("../app");

describe("GET /", function() {
    it("it should has status code 501", function(done) {
      supertest(app)
        .get("/")
        .expect(501)
        .end(function(err, res){
          if (err) done(err);
          done();
        });
    });
});