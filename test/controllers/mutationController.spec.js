const supertest = require("supertest");
const assert = require('assert');
const app = require("../../app");


//Remove dnaString 1
describe('DELETE /mutation/delete', () => {
    it('Should delete a dnaString',function(done) {
        supertest(app)
        .delete(`/mutation/${["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"].join('')}`)
        .expect(200)
        .end(function(err, res){
            if (err) done(err);
            done();
        });
    });
});

//Remove dnaString 2
describe('DELETE /mutation/delete', () => {
    it('Should delete a dnaString',function(done) {
        supertest(app)
        .delete(`/mutation/${["ATGCGA","CAGTGC","TTATTT","AGACGG","GCGTCA","TCACTG"].join('')}`)
        .expect(200)
        .end(function(err, res){
            if (err) done(err);
            done();
        });
    });
});

// check mutation in dna and save
describe("POST /", function() {
    it("it should has status code 200", function(done) {
      supertest(app)
        .post("/mutation")
        .send({
            "dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
        })
        .expect(200)
        .end(function(err, res){
          if (err) done(err);
          done();
        });
    });
});

// check mutation in previously saved dna 
describe("POST /", function() {
    it("it should has status code 200", function(done) {
      supertest(app)
        .post("/mutation")
        .send({
            "dna":["ATGCGA","CAGTGC","TTATTT","AGACGG","GCGTCA","TCACTG"]
        })
        .expect(403)
        .end(function(err, res){
          if (err) done(err);
          done();
        });
    });
});

// check no mutation in previously saved dna 
describe("POST /", function() {
    it("it should has status code 200", function(done) {
      supertest(app)
        .post("/mutation")
        .send({
            "dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
        })
        .expect(200)
        .end(function(err, res){
          if (err) done(err);
          done();
        });
    });
});
describe("POST /", function() {
    it("it should has status code 200", function(done) {
      supertest(app)
        .post("/mutation")
        .send({
            "dna":["ATGCGA","CAGTGC","TTATTT","AGACGG","GCGTCA","TCACTG"]
        })
        .expect(403)
        .end(function(err, res){
          if (err) done(err);
          done();
        });
    });
});