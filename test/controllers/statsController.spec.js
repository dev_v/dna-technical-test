const supertest = require("supertest");
const assert = require('assert');
const app = require("../../app");


describe("GET /", function() {
    it("it should return a json", function(done) {
      supertest(app)
        .get("/stats")
        .expect('Content-Type', /json/)
        .end(function(err, res){
          if (err) done(err);
          done();
        });
    });
});