const assert = require('assert');
const { hasMutation, findWordsHelperH } = require('../../helpers/mutations');


describe("Testing mutation dna", function() {
    it("Should be return false for a dna string without mutation", function() {
        assert.strictEqual(hasMutation(["ATGCGA","CAGTGC","TTATTT","AGACGG","GCGTCA","TCACTG"]),false,"DNA correctly tested (no mutation)","DNA incorrectly tested (False positive)");
    });
});

describe("Testing mutation dna", function() {
    it("Should be return true for a dna string with mutation", function() {
        assert.strictEqual(hasMutation(["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]),true,"DNA correctly tested (mutation)","DNA incorrectly tested (False negative)");
    });
});

describe("Testing DNA mutation found vertically (uptodown)", function() {
    it("Should be return true for a dna string with mutation", function() {
        assert.strictEqual(hasMutation([
            "ATGCGA", 
            "CAGTGC", 
            "TTATTT", //T 
            "AGATGG", //T
            "GCGTCA", //T
            "TCATTG"  //T
        ]),true,"DNA correctly tested (mutation)","DNA incorrectly tested (False negative)");
    });
});

describe("Testing DNA mutation found horizontally (lefttoright)", function() {
    it("Should be return true for a dna string with mutation", function() {
        assert.strictEqual(hasMutation([
            "ATGCGA", 
            "CAGTGC", 
            "TTATTT", 
            "AGAAGG", 
            "CCCCTA", // CCCC 
            "TCACTG" 
        ]),true,"DNA correctly tested (mutation)","DNA incorrectly tested (False negative)");
    });
});

describe("Testing DNA mutation found diagonally (TopLeftToBottomRight)", function() {
    it("Should be return true for a dna string with mutation", function() {
        assert.strictEqual(hasMutation(["ATGCGA"
        ,"CAGTGC"//C
        ,"TCATTT"// C
        ,"AGCCGG"//  C
        ,"GCGCCA"//   C
        ,"TCACTG"
        ]),true,"DNA correctly tested (mutation)","DNA incorrectly tested (False negative)");
    });
});

describe("Testing DNA mutation found diagonally (TopRightToBottomLeft)", function() {
    it("Should be return true for a dna string with mutation", function() {
        assert.strictEqual(hasMutation([
            "ATGCGA",//     A
            "CAGTAC",//    A
            "TTAATT",//   A
            "AGACGG",//  A
            "GCGTCA",
            "TCACTG"
        ]),true,"DNA correctly tested (mutation)","DNA incorrectly tested (False negative)");
    });
});