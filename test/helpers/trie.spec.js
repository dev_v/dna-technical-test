const assert = require('assert');
const { Trie } = require('../../helpers/trie');


describe("Testing data structure Trie", function() {
    it("Should be create a tree", function() {
        let trie = new Trie();
        assert.strictEqual(trie.root.value,undefined,"root is empty","root isnt empty");
        assert.strictEqual(trie.root.isEnd,false,"root isnt the end","root is the end");
    });
});